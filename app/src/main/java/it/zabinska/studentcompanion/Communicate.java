package it.zabinska.studentcompanion;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

/**
 * Klasa Communicate
 * Tworzy połączenie z serwerem poprzez socket TCP.
 * Przesyła wiadomości z żądaniami klienta do serwera.
 */
public class Communicate {

    private static Socket sock;
    static String host = "192.168.0.6";
    static int portNumber = 8181;
    public static boolean isServerRunning() throws IOException {

        try (Socket s = new Socket(host, portNumber)) {
            s.close();
            return true;
        } catch (IOException ex) {
        System.err.println("Nie połączono. Upewnij się czy serwer jest uruchomiony i spróbuj ponownie.");
        }
        return false;
    }

    public static Boolean createConnection() throws IOException {
        Boolean status = false;
        try {
            System.out.println("Tworzenie socketu do hosta '" + host + "' na porcie " + portNumber);
            sock = new Socket(host, portNumber);
            System.out.println("Pomyślnie utworzono połączenie z serwerem.");
            status = true;
        } catch (Exception e) {
            System.err.println("Nie połączono. Upewnij się czy serwer jest uruchomiony i spróbuj ponownie.");
        }
        return status;
    }

    public static void createOutputStream(String message) throws IOException {
        try {
            DataOutputStream outToServer = new DataOutputStream(sock.getOutputStream());
            outToServer.writeBytes(message + '\n');
            outToServer.flush();

        } catch (IOException e) {
            System.err.println("Nie udało się wysłać wiadomości. Możliwe, że nie wystąpił błąd podczas tworzenia strumienia. Sprawdź poniższy błąd, by dowiedzieć się wiecej.");
            System.err.print(e);
        }
    }

    /**
     * Funkcja public static Boolean getValidation(Byte messageType, String messageString)
     * Zawiera switch zależny od argumentu messageType. Wysyła do serwera żądania poprzez wiadomość w argumencie messageString.
     * Zwracany Bool informuje o wyniku wywołania zapytania SQL w bazie danych, dlatego używana jest do wysyłania żądań sprawdzajacyh, niemodyfikujących bazy typu SELECT.
     * @param messageType typ identyfikujący switch(case)
     * @param messageString treść żadania wysyłanego do serwera.
     * @return Boolean, odpowiedź od serwera na wysłane żądanie.
     */
    public static Boolean getValidation(Byte messageType, String messageString) throws IOException {
        switch (messageType) {
            case 1://<editor-fold desc="isEmailRegistered">
            {
                System.out.println("Żądanie sprawdzające czy email " + messageString + " jest zarejestrowany w bazie.");
               createOutputStream(messageString);
                try {
                    BufferedReader inFromServer = new BufferedReader(new InputStreamReader(sock.getInputStream()));
                    String res = inFromServer.readLine();
                    System.out.println("Otrzymano odpowiedź od serwera. Treść wiadomości: " + res);
                    if (res.equals("false")) {
                        System.out.println("Email " + res + " nie jest zarejestrowany w bazie danych.");
                        return false;
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
                return true;
            }//</editor-fold>
            case 2://<editor-fold desc="isPasswordCorrect">
            {
                System.out.println("Żądanie sprawdzające czy wprowadzone hasło zgadza się z hasłem zapisanym w bazie danych.");
                createOutputStream(messageString);
                try {
                    BufferedReader inFromServer = new BufferedReader(new InputStreamReader(sock.getInputStream()));
                    String res;
                    res = inFromServer.readLine();
                    System.out.println("Otrzymano odpowiedź od serwera. Treść wiadomości: " + res);
                    if (!Boolean.valueOf(res)) {
                        System.out.println("Password match: " + res);
                        return false;
                    }
                } catch (IOException e) {
                    System.out.println(e);
                } catch (Exception e) {
                    System.out.println(e);
                }
                return true;
            }//</editor-fold>
            case 3://<editor-fold desc="isUsernameFree">
            {
                System.out.println("Żądanie sprawdzające czy nazwa użytkownika: " + messageString + " nie jest zajęta.");
                createOutputStream(messageString);
                try {
                    BufferedReader inFromServer = new BufferedReader(new InputStreamReader(sock.getInputStream()));
                    String res;
                    res = inFromServer.readLine();
                    System.out.println("Otrzymano odpowiedź od serwera. Treść wiadomości: " + res);
                    String result = res.toString();
                    if (!Boolean.valueOf(result)) {
                        System.out.println("Username is taken: " + res);
                        return false;
                    }

                } catch (IOException e) {
                    System.out.println(e);
                } catch (Exception e) {
                    System.out.println(e);
                }
                return true;
            }//</editor-fold>
            case 4://<editor-fold desc="sendPassword">
            {
                System.out.println("Żądanie rejestrujace użytkownika w bazdie danych.");
                createOutputStream(messageString);
                try {
                    //Sending message to server
                    DataOutputStream outToServer = new DataOutputStream(sock.getOutputStream());
                    outToServer.writeBytes(messageString + '\n');
                    outToServer.flush();
                } catch (IOException e) {
                    System.err.print(e);
                }
                try {
                    BufferedReader inFromServer = new BufferedReader(new InputStreamReader(sock.getInputStream()));
                    String res;
                    res = inFromServer.readLine();
                    System.out.println("Received: " + res);
                    String result = res.toString();
                    System.out.println(Boolean.valueOf(result));
                    if (!Boolean.valueOf(result)) {
                        System.out.println("Registration status: " + res);
                        return false;
                    }
                } catch (IOException e) {
                    System.out.println(e);
                } catch (Exception e) {
                    System.out.println(e);
                }
                return true;
            }//</editor-fold>
            case 5://<editor-fold desc="puste">
            {
                break;
            }//</editor-fold>
            case 6://<editor-fold desc = "puste">
            {
                break;
            }//</editor-fold>
            case 7://<editor-fold desc = "puste">
            {
                break;
            }//</editor-fold>
            case 8://<editor-fold desc = "puste">
            {
                break;
            }
            //</editor-fold>
        }
        return false;
    }
    public static void getData(byte message_type, String message) throws IOException {
        switch (message_type){

            case 1: //<editor-fold desc="whatIsUsername">
            {
                System.out.println("Żądanie sprawdzające jaka jest nazwa użytkownika dla maila: " + message + ".");
                createOutputStream(message);
                String res="NULL";
                try {
                    BufferedReader inFromServer = new BufferedReader(new InputStreamReader(sock.getInputStream()));
                    res = inFromServer.readLine();
                    ProvisionalValidation.username = res;
                    System.out.println("Login: " + res);
                } catch (Exception e) {
                    System.out.println(e);
                }break;
            }
            //</editor-fold>
            case 2: //<editor-fold desc="whatIsUserID">
                {
                    System.out.println("Żądanie sprawdzające jakie jest id logującego się użytkownika.");
                    Communicate.createOutputStream(message);
                    try {
                        BufferedReader inFromServer = new BufferedReader(new InputStreamReader(sock.getInputStream()));
                        String res;
                        res = inFromServer.readLine();
                        System.out.println("Otrzymano odpowiedź od serwera. Treść wiadomości: " + res);
                        ProvisionalValidation.userid = Integer.parseInt(res);
                    } catch (IOException e) {
                        System.out.println(e);
                    } catch (Exception e) {
                        System.out.println(e);
                    }
                    break;
                }
            //</editor-fold>
            case 3: //<editor-fold desc="showGrades">
            {
                System.out.println("Żądanie sprawdzające oceny użytkownika.");
                createOutputStream(message);
                try {
                    //Odczytujemy informacje o ilości ocen
                    BufferedReader inFromServerAmount = new BufferedReader(new InputStreamReader(sock.getInputStream()));
                    String gradesAmount = inFromServerAmount.readLine();
                    //Tworzymy odpowiednio dużą tablicę
                    ProvisionalValidation.gAmount = Integer.parseInt(gradesAmount);
                    ProvisionalValidation.grades = new String[ProvisionalValidation.gAmount][3];

                    int i = 0;

                    //Odpowiadamy posząc o oceny
                    String uidString = Integer.toString(ProvisionalValidation.userid);
                    message = "5" + uidString;
                    createOutputStream(message);

                    //Odczytujemy oceny
                    BufferedReader inFromServerGrades = new BufferedReader(new InputStreamReader(sock.getInputStream()));
                    String grades = inFromServerGrades.readLine();
                    while ((grades != null)) {

                        System.out.println("Odczyt ocen numer: " + i);
                        String messageline = grades.toString();
                        System.out.println("Parsowanie udane");
                        ProvisionalValidation.grades[i][0] = messageline.substring(0, 3);
                        System.out.println( i + ". Ocena: " + ProvisionalValidation.grades[i][0]);
                        ProvisionalValidation.grades[i][1] = messageline.substring(3, 6);
                        System.out.println( i + ". Waga: " + ProvisionalValidation.grades[i][1]);
                        ProvisionalValidation.grades[i][2] = messageline.substring(6, messageline.length());
                        System.out.println( i + ". Przedmiot: " + ProvisionalValidation.grades[i][2]);
                        i++;
                        System.out.println("Grades"  + grades);
                        System.out.println("i"  + i);
                        System.out.println("gAmount"  + ProvisionalValidation.gAmount);
                        if(i!=ProvisionalValidation.gAmount)
                        {
                            grades = inFromServerGrades.readLine();
                        }
                        else{
                            grades = null;
                        }
                    }
                    if(i==ProvisionalValidation.gAmount) {
                        System.out.println("Koniec ocen!");
                    }
                }
                catch(Exception e){
                    System.err.println("Nie ma żadnych ocen!");
                    System.err.println(e);
                }
                break;
            }
            //</editor-fold>
            case 4: //<editor-fold desc="showTasks">
            {
                System.out.println("Żądanie sprawdzające zadania użytkownika.");
                createOutputStream(message);
                try {
                    //Odczytujemy informacje o ilości zadań
                    BufferedReader inFromServerAmount = new BufferedReader(new InputStreamReader(sock.getInputStream()));
                    String tasksAmount = inFromServerAmount.readLine();
                    //Tworzymy odpowiednio dużą tablicę
                    ProvisionalValidation.tAmount = Integer.parseInt(tasksAmount);
                    ProvisionalValidation.tasks = new String[ProvisionalValidation.tAmount][2];

                    int i = 0;
                    //Odpowiadamy posząc o zadania
                    String uidString = Integer.toString(ProvisionalValidation.userid);
                    message = "a12" + uidString;
                    createOutputStream(message);

                    //Odczytujemy zadania
                    BufferedReader inFromServer = new BufferedReader(new InputStreamReader(sock.getInputStream()));
                    String tasks = inFromServer.readLine();

                    while ((tasks != null)) {
                        System.out.println("Odczyt zadania numer: " + i);
                        String messageline = tasks.toString();
                        System.out.println("Parsowanie udane");
                        ProvisionalValidation.tasks[i][0] = messageline.substring(0, 1);
                        System.out.println( i + ". Status: " + ProvisionalValidation.tasks[i][0]);
                        ProvisionalValidation.tasks[i][1] = messageline.substring(1, messageline.length());
                        System.out.println( i + ". Zadanie: " + ProvisionalValidation.tasks[i][1]);
                        i++;
                        if(i!=ProvisionalValidation.tAmount)
                        {
                            tasks = inFromServer.readLine();
                        }
                        else{
                            tasks=null;
                        }
                    }
                    System.out.println("Koniec zadań!");
                }
                catch(Exception e){
                    System.err.println("Nie ma żadnych zadań!");
                    System.err.println(e);
                }
                break;
            }//</editor-fold>
            case 5: //<editor-fold desc="showSubjects">
            {
                System.out.println("Żądanie sprawdzające przedmioty użytkownika");
                createOutputStream(message);
                try {
                //Odczytujemy informacje o ilości przedmiotów
                BufferedReader inFromServerAmount = new BufferedReader(new InputStreamReader(sock.getInputStream()));
                String subjectsAmount = inFromServerAmount.readLine();
                //Tworzymy odpowiednio dużą tablicę
                ProvisionalValidation.sAmount = Integer.parseInt(subjectsAmount);
                ProvisionalValidation.subjects = new String[ProvisionalValidation.sAmount];

                int i = 0;

                //Odpowiadamy posząc o zadania
                String uidString = Integer.toString(ProvisionalValidation.userid);
                message = "8" + uidString;
                createOutputStream(message);
                System.out.println("Ilosc przedmiotów:" + ProvisionalValidation.sAmount);
                //Odczytujemy zadania
                BufferedReader inFromServer = new BufferedReader(new InputStreamReader(sock.getInputStream()));
                String subjects = inFromServer.readLine();

                while ((subjects != null)) {
                    System.out.println("Odczyt przedmiotu numer: " + i);
                    ProvisionalValidation.subjects[i] = subjects.toString();
                    System.out.println("Parsowanie udane");
                    System.out.println( i + ". Przedmiot: " + ProvisionalValidation.subjects[i]);
                    i++;
                    if(i!=ProvisionalValidation.sAmount)
                    {
                        subjects = inFromServer.readLine();
                    }
                    else{
                        subjects=null;
                    }
                }
                System.out.println("Koniec przedmiotów!");
            }
            catch(Exception e){
                System.err.println("Nie ma żadnych przedmiotów!");
                System.err.println(e);
            }
                break;
            }//</editor-fold>
            case 6: //<editor-fold desc="bindSubjectToUser">
            {
                System.out.println("Żądanie przypisujące przedmiot użytkownikowi");
                createOutputStream(message);
                break;
            }//</editor-fold>
            case 7: //<editor-fold desc="addGrade">
            {

                System.out.println("Żądanie dodania do bazy danych nowej oceny");
                createOutputStream(message);
                closeConnection();
                break;
            }//</editor-fold>
            case 8: //<editor-fold desc="getSubjects">
            {
                createOutputStream(message);
                try {
                    //Odczytujemy informacje o ilości przedmiotów
                    BufferedReader inFromServerAmount = new BufferedReader(new InputStreamReader(sock.getInputStream()));
                    String subjectsAmount = inFromServerAmount.readLine();
                    //Tworzymy odpowiednio dużą tablicę
                    ProvisionalValidation.sAmountA = Integer.parseInt(subjectsAmount);
                    ProvisionalValidation.subjectsall = new String[ProvisionalValidation.sAmountA];
                    int i = 0;
                    //Odpowiadamy posząc o zadania
                    message = "a17" + '\n';
                    createOutputStream(message);
                    System.out.println("Ilosc przedmiotów:" + ProvisionalValidation.sAmountA);

                    //Odczytujemy zadania
                    BufferedReader inFromServer = new BufferedReader(new InputStreamReader(sock.getInputStream()));
                    String subjects = inFromServer.readLine();

                    while ((subjects != null)) {
                        System.out.println("Odczyt przedmiotu numer: " + i);
                        ProvisionalValidation.subjectsall[i] = subjects.toString();
                        System.out.println("Parsowanie udane");
                        System.out.println( i + ". Przedmiot: " + ProvisionalValidation.subjectsall[i]);
                        i++;
                        if(i!=ProvisionalValidation.sAmountA)
                        {
                            subjects = inFromServer.readLine();
                        }
                        else{
                            subjects=null;
                        }
                    }
                    System.out.println("Koniec przedmiotów!");
                }
                catch(Exception e){
                    System.err.println("Nie ma żadnych przedmiotów!");
                    System.err.println(e);
                }
                break;
            }//</editor-fold>
        }
    }
    public static void closeConnection() {
        try {
            sock.close();
            System.out.println("Zamknięto socket! ");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
package it.zabinska.studentcompanion;

/**
 * Klasa DatabaseQueries zawiera funkcje opisujące wiadomości wysyłane do serwera przez funkcje getData oraz getValidation znajdujące sie w klasie Communicate.
 * Funkcje, które mają za zadanie przeprowadzić walidację wywołują funkcję getValidation.
 * Jeśli potrzebne sa konkretne dane z bazy danych - używane na przykład do wyświetlenia czegoś w aplkacji uzywana jest funkcja getData.
 */
public class DatabaseQueries {

    //getData methods:
    /**
     * Wyciąganie nazyw użytkownika na podstawie wprowadzonego adresu email.
     * Zwraca void, ponieważ String z odpowiedzią jest prrzypisywany na etapie wywoływania funkcji.
     * @param email
     */
    public static void whatIsUsername(String email) {
        Byte type = 1;
        try {
            String message = "a13" + email;
            Communicate.getData(type, message);
            System.out.println("username: " + ProvisionalValidation.username);

        } catch (Exception e) {
            System.err.print(e);
        }
        //  Communicate.closeConnection();
    }
    public static void whatIsUserID(String email) {
        Byte type = 2;
        try {
            String message = "a16" + email;
            Communicate.getData(type, message);
            System.out.println("UserID: " + ProvisionalValidation.userid);

        } catch (Exception e) {
            System.err.print(e);
        }
        //  Communicate.closeConnection();
    }
    /**
     * Wyciąganie ocen danego użytkownika na podstawie adresu email;
     * Zwraca void, ponieważ tablica Stringów zawierająca oceny - ProvisionalValidation.grades[][] jest wypełniana na etapie odbioru wiadomości od serwera.
     *
     * @param userid
     */
    //TODO: Poprawić odzyskiwanie tego stringa
    public static void showGrades(int userid){
        Byte type = 3;
        try {
            String uidString = Integer.toString(userid);
            String message = "9" + uidString;
            Communicate.getData(type, message);

        } catch (Exception e) {
            System.err.print(e);
        }
    }
    public static void showTasks(int userid){
        Byte type = 4;
        try {
            String uidString = Integer.toString(userid);
            String message = "a10" + uidString;
            Communicate.getData(type, message);

        } catch (Exception e) {
            System.err.print(e);
        }
    }
    public static void showSubjects(int userid){
        Byte type = 5;
        try {
            String uidString = Integer.toString(userid);
            String message = "a14" + uidString;
            Communicate.getData(type, message);

        } catch (Exception e) {
            System.err.print(e);
        }
    }
    public static void bindSubjectToUser(String subject)
    {
        Byte type = 6;
        try{
            String uid = Integer.toString(ProvisionalValidation.userid);
            String message = "7" + uid + subject;
            Communicate.getData(type, message);
            System.out.println("Przypisano przedmiot");
        }
        catch(Exception e){
            System.err.print(e);
        }

    }
    public static void addGrade(String gradeweight, String gradevalue, String gradesubject) {
        Byte type = 7;
        try{
            String uidtype="";
            String uid = Integer.toString(ProvisionalValidation.userid);
            if(uid.length()>1){
                uidtype="w";
            }
            else{
                uidtype="q";
            }
            String message = "6" + uidtype + uid + gradeweight + gradevalue + gradesubject;
            Communicate.getData(type, message);
        } catch(Exception e){
            System.err.println(e);
        }
    }
   //getValidation methods:
    /**
     *
     * @param email
     * Walidacja maila w bazie danych
     * @return
     */
    public static boolean isEmailRegistered(String email) {
        Boolean toContinue = false;
        Byte type = 1;
        try {
            System.out.println("Wysłano wiadomość!");
            String message = "1" + email;
            toContinue = Communicate.getValidation(type, message);
            System.out.println(toContinue);
        }
        catch(Exception e)
        {
            System.err.print(e);
        }
        System.out.println(toContinue.toString());
        return toContinue;
    }
    /**
     *
     * @param password
     * Walidacja hasła w bazie danych
     * @return
     */
    public static boolean isPasswordCorrect(String password) {
        Boolean toContinue = false; //true for debug purposes
        Byte type = 2;
        try {
            String hashPassword = PassSHA.hash(password);
            System.out.println(hashPassword);
            System.out.println("Wysłano wiadomość!");
            String message = "2" + hashPassword;
            toContinue = Communicate.getValidation(type, message);
        }
        catch(Exception e)
        {
            System.err.print(e);
        }
        System.out.println(toContinue.toString());
        return toContinue;
    }
    public static boolean isUsernameFree(String username) {
        Boolean toContinue = false;
        Byte type = 3;
        try {
            System.out.println("Wysłano username!");
            String message = "3" + username;
            toContinue = Communicate.getValidation(type, message);
            System.out.println(toContinue);
        }
        catch(Exception e)
        {
            System.err.print(e);
        }
        System.out.println(toContinue.toString());
        return toContinue;
    }
    public static boolean sendPassword(String password) {
        Boolean toContinue=false;
        Byte type = 4;
        try {
            System.out.println("Rejestruję");
            String message = "4" + PassSHA.hash(password);
            toContinue = Communicate.getValidation(type, message);
            System.out.println("Udana rejestracja: " + toContinue);
        }
        catch(Exception e)
        {
            System.err.print(e);
        }
        System.out.println(toContinue.toString());
        return toContinue;
    }

}

package it.zabinska.studentcompanion;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.style.UpdateLayout;
import android.view.View;
import android.widget.AbsSpinner;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;

public class GradeActivity extends AppCompatActivity {
    String gradevalueString;
    String gradeweightString;
    String subjectString;
    int lastadded = ProvisionalValidation.sAmount;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grade);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        ArrayAdapter<String> subjectsArray= new ArrayAdapter<String>(GradeActivity.this,android.R.layout.simple_spinner_item, ProvisionalValidation.subjects);
        subjectsArray.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        final Spinner subjects = (Spinner) findViewById(R.id.subjectSpinner);
        subjects.setAdapter(subjectsArray);
        updateUI();

        Button newGrade = (Button) findViewById(R.id.grade_btn_add);
        newGrade.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                subjectString = subjects.getSelectedItem().toString();
                EditText grade_weight = (EditText) findViewById(R.id.grade_weight_txt);
                gradeweightString = grade_weight.getText().toString();
                float gradeweight = Float.parseFloat(gradeweightString);
                if (ProvisionalValidation.isGradeWeightValid(gradeweight)) {
                    EditText grade_value = (EditText) findViewById(R.id.grade_value_txt);
                    gradevalueString = grade_value.getText().toString();
                    Float gradeValue = Float.parseFloat(gradevalueString);
                    if (ProvisionalValidation.isGradeValid(gradeValue)) {
                        UserGradeTask ugt = new UserGradeTask();
                        ugt.execute();
                        grade_weight.setText("");
                        grade_value.setText("");
                    } else {
                        grade_value.setError("Ocena powinna być między 2.0 a 5.0");
                        Toast.makeText(getApplicationContext(), "Zła ocena!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                } else {
                    grade_weight.setError("Waga oeny powinna być między 0.01 a 1.0");
                    Toast.makeText(getApplicationContext(), "Zła waga!", Toast.LENGTH_SHORT).show();
                    return;
                }
            }
        });
    updateUI();
    }

    void updateUI()
    {
        TableLayout table = (TableLayout) findViewById(R.id.tableGrades);
        System.out.println("Lstad: " + lastadded);
        if(lastadded!=0){
        for(int i=0; i<lastadded; i++){
            table.removeView(findViewById(666+i));
        }}
        for(int i=0; i<ProvisionalValidation.gAmount; i++) {
            TableRow row = new TableRow(this);
            row.setId(666+i);
            row.setLayoutParams(new TableRow.LayoutParams());
            TextView przedmiot = new TextView(this);
            przedmiot.setText(ProvisionalValidation.grades[i][2]);
            row.addView(przedmiot);
            TextView waga = new TextView(this);
            waga.setText(ProvisionalValidation.grades[i][1]);
            row.addView(waga);
            TextView ocena = new TextView(this);
            ocena.setText(ProvisionalValidation.grades[i][0]);
            row.addView(ocena);
            table.addView(row);
            lastadded++;
        }
    }
    public class UserGradeTask extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                Boolean c = Communicate.createConnection();
                if (c) {
                    try {
                        DatabaseQueries.addGrade(gradeweightString, gradevalueString, subjectString);
                        Communicate.closeConnection();
                        Communicate.createConnection();
                        DatabaseQueries.showGrades(ProvisionalValidation.userid);
                        Communicate.closeConnection();
                        return true;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            return false;
        }

        protected void onPostExecute(Boolean s) {
            if (s) {
                updateUI();
            } else {
                Toast.makeText(getApplicationContext(), "Nie udalo sie dodac oceny!", Toast.LENGTH_SHORT).show();
            }
        }
    }
}

package it.zabinska.studentcompanion;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.ActionBar;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.app.LoaderManager.LoaderCallbacks;

import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;

import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static android.Manifest.permission.READ_CONTACTS;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity implements LoaderCallbacks<Cursor> {

    /**
     * Id to identity READ_CONTACTS permission request.
     */
    private static final int REQUEST_READ_CONTACTS = 0;


    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
    private UserLoginTask mAuthTask = null;

    // UI references.
    private AutoCompleteTextView userEmail;
    AutoCompleteTextView userName;
    EditText userPass;
    private View mProgressView;
    private View mLoginFormView;
    String emailString;
    String passString;
    View focusView = null;
    boolean cancel = false;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setupActionBar();

        userEmail = (AutoCompleteTextView) findViewById(R.id.userName);
        userName = (AutoCompleteTextView) findViewById(R.id.userName);
        userPass = (EditText) findViewById(R.id.UserPass);
        populateAutoComplete();
        Button loginButton = (Button) findViewById(R.id.LoginButton);
        loginButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                emailString = userEmail.getText().toString();
                Boolean isEmailValid= ProvisionalValidation.isEmailValid(emailString);
                if (isEmailValid) {
                    passString = userPass.getText().toString();
                    Boolean isPasswordValid = ProvisionalValidation.isPasswordValid(passString);
                    if (isPasswordValid) {
                        mAuthTask = new UserLoginTask(emailString, passString);
                        mAuthTask.execute((Void) null);
                    } else {
                        userPass.setError(getString(R.string.error_invalid_password));
                        focusView = userPass;
                        return;
                    }
                } else {
                    userEmail.setError(getString(R.string.error_invalid_email));
                    focusView = userEmail;
                    return;
                }
            }
        });

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);

        //Link do rejestracji
        TextView registerScreen = (TextView) findViewById(R.id.link_to_register);
        registerScreen.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                // Switching to Register screen
                Intent i = new Intent(getApplicationContext(), RegisterActivity.class);
                startActivity(i);
            }
        });
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    private void populateAutoComplete() {
        if (!mayRequestContacts()) {
            return;
        }
        getLoaderManager().initLoader(0, null, this);
    }

    private boolean mayRequestContacts() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (checkSelfPermission(READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        if (shouldShowRequestPermissionRationale(READ_CONTACTS)) {
            Snackbar.make(userName, R.string.permission_rationale, Snackbar.LENGTH_INDEFINITE)
                    .setAction(android.R.string.ok, new OnClickListener() {
                        @Override
                        @TargetApi(Build.VERSION_CODES.M)
                        public void onClick(View v) {
                            requestPermissions(new String[]{READ_CONTACTS}, REQUEST_READ_CONTACTS);
                        }
                    });
        } else {
            requestPermissions(new String[]{READ_CONTACTS}, REQUEST_READ_CONTACTS);
        }
        return false;
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_READ_CONTACTS) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                populateAutoComplete();
            }
        }
    }

    /**
     * Set up the {@link ActionBar}, if the API is available.
     */
    @TargetApi(Build.VERSION_CODES.KITKAT)
    private void setupActionBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            // Show the Up button in the action bar.
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    /**
     * Funkcja logująca. Wywołana zostaje gdy wiemy, że email ma poprawny format i hasło ma przynajmniej osiem znaków.
     * Potrzebne jest jedynie sprawdzenie, czy dany email istnieje w bazie danych oraz czy hasło, które wprowadził uzytkownik zadza się z hasłem w bazie danych dla danego maila.
     */
    private void login() {
        userName.setError(null);
        userPass.setError(null);
        focusView = userEmail;
        /**
         * Sprawdzamy czy proces logowania przebiegł bez błędów.
         * Jeśli cancel = true, oznacza to, że wystąpiły jakieś błędy i proces logowania zostanie przerwany. Użytkownik zostanie poproszony o poprawienie danych.
         * Jesli cancel = false, oznacza to, że nie wystąpił żadne błędy w procesie logowania i zostanie otworzona kolejna karta - Overview.
         */
        if (cancel) {
            System.err.println("Logowanie nieudane. Spróbuj ponownie.");
//            Communicate.closeConnection();
            cancel = false;
            focusView.requestFocus();
            return;

        } else {
            showProgress(true);
            Communicate.closeConnection();
            ProvisionalValidation.email = emailString;
            Intent intentOverview = new Intent(LoginActivity.this, Overview.class);
            startActivity(intentOverview);
            finish();
        }
    }


    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        return new CursorLoader(this,
                // Retrieve data rows for the device user's 'profile' contact.
                Uri.withAppendedPath(ContactsContract.Profile.CONTENT_URI,
                        ContactsContract.Contacts.Data.CONTENT_DIRECTORY), ProfileQuery.PROJECTION,

                // Select only email addresses.
                ContactsContract.Contacts.Data.MIMETYPE +
                        " = ?", new String[]{ContactsContract.CommonDataKinds.Email
                .CONTENT_ITEM_TYPE},

                // Show primary email addresses first. Note that there won't be
                // a primary email address if the user hasn't specified one.
                ContactsContract.Contacts.Data.IS_PRIMARY + " DESC");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        List<String> emails = new ArrayList<>();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            emails.add(cursor.getString(ProfileQuery.ADDRESS));
            cursor.moveToNext();
        }

        addEmailsToAutoComplete(emails);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {

    }

    private void addEmailsToAutoComplete(List<String> emailAddressCollection) {
        //Create adapter to tell the AutoCompleteTextView what to show in its dropdown list.
        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(LoginActivity.this,
                        android.R.layout.simple_dropdown_item_1line, emailAddressCollection);

        userEmail.setAdapter(adapter);
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Login Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://it.zabinska.studentcompanion/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Login Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://it.zabinska.studentcompanion/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }


    private interface ProfileQuery {
        String[] PROJECTION = {
                ContactsContract.CommonDataKinds.Email.ADDRESS,
                ContactsContract.CommonDataKinds.Email.IS_PRIMARY,
        };

        int ADDRESS = 0;
        int IS_PRIMARY = 1;
    }

    public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {

        private final String mEmail;
        private final String mPassword;

        UserLoginTask(String email, String password) {
            mEmail = email;
            mPassword = password;
        }

        Boolean s = false;
        Boolean l = false;
        Boolean c = false;

        /**
         * Sprawdzenie czy isstnieje połączneie z serwerem, zwalidowanie adresu email oraz hasła z bazą danych oraz wycignięcie z bazy danych nazwy użytkownika, który sie loguje.
         * Jeśli walidacje oraz połączenie z serwerem sie powiodą następuje start nowej karty - overview.
         * doInBackground realizuje AsyncTask, zwrócone Booleany są odbierane przez funkcję onPostExecute
         * @param params
         * @return
         */
        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                //Checking if server is running
                if (Communicate.isServerRunning()) {
                    try {
                        s = Communicate.createConnection();
                    } catch (IOException e) {
                        Toast.makeText(getApplicationContext(), "Serwer nie odpowiada!", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    System.err.println("Serwer nie odpowiada.");
                }
            } catch (IOException e) {
                System.err.println("Blad połączenia z serwerem!");
                Toast.makeText(getApplicationContext(), "Serwer nie odpowiada!", Toast.LENGTH_SHORT).show();
            }
            if (s) {
                l = DatabaseQueries.isEmailRegistered(mEmail);
                if (l) {
                    DatabaseQueries.whatIsUsername(mEmail);
                    DatabaseQueries.whatIsUserID(mEmail);
                        c = DatabaseQueries.isPasswordCorrect(mPassword);

                } else {
                    return l; }
            } else {
                return s; }
            return c;
        }

        /**
         * onPostExecute sprawdza czy wygstąpiły jakiekolwiek przeszkody w procesie logowania. Jeśli tak - ustawia cancel na true oraz resetuje zmienne. Następnie wywołuje funkcję login();
         * @param s
         */
        @Override
        protected void onPostExecute(final Boolean s) {
            if (!l){
                Toast.makeText(getApplicationContext(), "Adres email jest niezarejestrowany. Zarejestruj się!", Toast.LENGTH_SHORT).show();
                cancel = true;
                l=true;
            }
            else if(!c) {
                Toast.makeText(getApplicationContext(), "Błędne hasło!", Toast.LENGTH_SHORT).show();
                cancel = true;
                c=true;
            }
            else if(!s){
                Toast.makeText(getApplicationContext(), "Błąd połaczenia", Toast.LENGTH_SHORT).show();
                cancel = true;
            }
            login();
        }

        /**
         * Funkcja resetująca obiekt UserLoginTask
         */
        @Override
        protected void onCancelled() {
            mAuthTask = null;
            showProgress(false);
        }
    }
}



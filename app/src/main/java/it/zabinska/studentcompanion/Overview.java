package it.zabinska.studentcompanion;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Layout;
import android.text.style.UpdateLayout;
import android.view.View;
import android.view.Menu;
import android.content.Intent;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.IOException;

public class Overview extends AppCompatActivity {

    public final static String EXTRA_MESSAGE = "it.zabinska.StudentCompanion.MESSAGE";
    Boolean a = false;
    Boolean b = false;
    Boolean d = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        System.out.println("Username: " + ProvisionalValidation.username);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_overview);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        TextView welcome_message = new TextView(this);
        welcome_message=(TextView)findViewById(R.id.welcome_ov);
        welcome_message.setText("Witaj, " + ProvisionalValidation.username);

        Button gradeButton = (Button) findViewById(R.id.GradeButton);
        gradeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                a = true;
                b = false;
                d = false;
                UserOverviewTask uot = new UserOverviewTask();
                uot.execute();
            }
        });

        Button taskButton = (Button) findViewById(R.id.TaskButton);
        taskButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                b = true;
                a = false;
                d = false;
                UserOverviewTask uot = new UserOverviewTask();
                uot.execute();
            }
        });

        Button subjectButton = (Button) findViewById(R.id.SubjectButton);
        subjectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                d = true;
                a = false;
                b = false;
                UserOverviewTask uot = new UserOverviewTask();
                uot.execute();
            }
        });
    }

    public class UserOverviewTask extends AsyncTask<Void, Void, Boolean> {
        Boolean g = false;
        Boolean t = false;
        Boolean s = false;
        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                Boolean c = Communicate.createConnection();
                if(c && a){
                    try{

                        DatabaseQueries.showGrades(ProvisionalValidation.userid);
                        Communicate.closeConnection();
                        Communicate.createConnection();
                        DatabaseQueries.showSubjects(ProvisionalValidation.userid);
                        Communicate.closeConnection();
                        g=true;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if(c && b){
                    try{
                        DatabaseQueries.showTasks(ProvisionalValidation.userid);
                        Communicate.closeConnection();
                        t=true;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if(c && d){
                    try{
                        Byte p=8;
                        Communicate.getData(p,"a15");
                        Communicate.closeConnection();
                        Communicate.createConnection();
                        DatabaseQueries.showSubjects(ProvisionalValidation.userid);
                        Communicate.closeConnection();
                        Communicate.createConnection();
                        DatabaseQueries.showGrades(ProvisionalValidation.userid);
                        Communicate.closeConnection();
                       // ProvisionalValidation.getAverage();
                        s=true;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
               // Communicate.createConnection();
               // DatabaseQueries.showGrades(ProvisionalValidation.email);

            } catch (IOException e1) {
                e1.printStackTrace();
            }

            return true;
        }
        protected void onPostExecute(Boolean r){
            if(s) {
                Intent i = new Intent(getApplicationContext(), SubjectActivity.class);
                startActivity(i);
            }
            if(t){
                Intent i = new Intent(getApplicationContext(), TaskViewActivity.class);
                startActivity(i);
            }
            if(g){
                Intent i = new Intent(getApplicationContext(), GradeActivity.class);
                startActivity(i);
            }
        }
    }
}
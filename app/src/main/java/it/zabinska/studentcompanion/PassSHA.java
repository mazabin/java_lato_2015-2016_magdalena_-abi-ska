package it.zabinska.studentcompanion;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class PassSHA {

    public static String hash(String input) {

        String sha256 = null;
        //Zahashowanie hasła
        if(null == input)
            return null;
        try{
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            digest.update(input.getBytes(), 0, input.length());
            sha256 = new BigInteger(1, digest.digest()).toString(16);
        }
        catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return sha256;
    }
}


package it.zabinska.studentcompanion;

/**
 * Created by zabin on 08.06.2016.
 */
public class ProvisionalValidation {

    public static String email;
    public static String username;
    public static String[][] grades;
    public static String[][] tasks;
    public static String[] subjects;
    public static String[] subjectsall;
    public static int userid;
    public static int gAmount;
    public static int tAmount;
    public static int sAmount;
    public static int sAmountA;
    public static float[] average;
    public static float[][] gradef;
    public static Boolean user_suc;
    public static Boolean grade_suc;


    /**
     *
     * @param email
     * * Wstępna walidacja maila - ma "@" i "."
     * @return
     */
    public static boolean isEmailValid(String email) {
        if (email.contains("@") && email.contains(".")) {
            return true;
        } else {
            return false;
        }
    }

    /**
     *
     * @param password
     * Wstepna walidacja hasła - ma przynajmniej osiem znaków
     * @return
     */
    public static boolean isPasswordValid(String password) {
        if (password.length() < 8) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Walidacja nazyw użytkownika - ma przynajmniej trzy znaki
     * @param name
     * @return
     */
    public static boolean isNameValid(String name){
        if(name.length() >= 3){
            return false;
        }
        else{
            System.out.println("Nazwa musi miec przynajmneij trzy znaki!");
            return true;
        }
    }
    public static boolean isGradeValid(Float grade){
        if(grade >= 2.0 && grade <= 5.0){
            return true;
        }
        else{
            System.out.println("Ocena powinna być w przedziale od 2.0 do 5.0!");
            return false;
        }
    }
    public static boolean isGradeWeightValid(Float gradeWeight){
        if(gradeWeight > 0.0 && gradeWeight <= 1.0){
            return true;
        }
        else{
            System.out.println("Waga oceny musi się mieścić między 0.01 a 1.0!");
            return false;
        }
    }
    static void changeToFloat()
    {
        gradef = new float[gAmount][2];
        for(int i=0; i<gAmount; i++){
            System.out.println(grades[i][0]);
            System.out.println(grades[i][1]);
            gradef[i][0]=Float.parseFloat(grades[i][0]);
            gradef[i][1]=Float.parseFloat(grades[i][1]);
        }
    }
    public static void getAverage(){
        changeToFloat();
        average = new float[sAmount+1];
        for(int i=0; i<gAmount; i++)
        {
            average[0] += gradef[i][0]*gradef[i][1];
        }
        average[0] = average[0]/gAmount; //średnia ze wszystkich przedmiotów
        for(int i=1; i<=sAmount; i++)
        {
            String sname = subjects[i];
            int cnt = 0;
            for(int j=0; j<gAmount; j++){ //średnia z konkretnego przedmiotu
                if(grades[j][2].equals(sname))
                {
                    cnt++;
                    average[i]+=gradef[j][0]*gradef[j][1];
                }
                else{
                    average[i] = 0;
                }
                average[i]=average[i]/cnt; //nazwa przedmiotu i średnia z niego są na tym samym miejscu.
            }
        }
    }
}
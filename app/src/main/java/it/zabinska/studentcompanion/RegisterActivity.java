package it.zabinska.studentcompanion;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;

import static it.zabinska.studentcompanion.Communicate.isServerRunning;

public class RegisterActivity extends AppCompatActivity {

    UserRegisterTask mRegTask = null;
    EditText regEmail;
    EditText regPass;
    EditText regPassRepeated;
    EditText userName;
    String regPassString;
    String userNameString;
    String regEmailString;
    View focusView = null;
    boolean cancel = false;
    Boolean arePasswordsTheSame = false;
    Boolean registration_is_successful = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        Button registerButton = (Button) findViewById(R.id.email_sign_up_button);
        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                regEmail = (EditText) findViewById(R.id.EmailReg);
                regPass = (EditText) findViewById(R.id.PasswordReg);
                regPassRepeated = (EditText) findViewById(R.id.PasswordReapeted);
                userName = (EditText) findViewById(R.id.userName);
                arePasswordsTheSame = (regPass.getText().toString().equals(regPassRepeated.getText().toString()));

                if (!arePasswordsTheSame){
                    System.out.println("Hasła się nie zgadzają");
                    Toast.makeText(getApplicationContext(), "Podane hasła różnią się!", Toast.LENGTH_SHORT).show();
                    return;
                }
                else
                {
                    regEmailString = regEmail.getText().toString();
                    if (ProvisionalValidation.isEmailValid(regEmailString)) {
                        regPassString = regPass.getText().toString();
                        if (ProvisionalValidation.isPasswordValid(regPassString)) {
                            userNameString = userName.getText().toString();
                            if(ProvisionalValidation.isNameValid(userNameString)){
                            mRegTask = new UserRegisterTask(regEmailString, userNameString, regPassString);
                            mRegTask.execute((Void) null);}
                            else{
                                System.out.println("zła nazwa użytkownika");
                                Toast.makeText(getApplicationContext(), "Nazwa użytkownika musi mieć rzynajmnie trzy znaki!", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            System.out.println("Nieporawne hasło");
                            Toast.makeText(getApplicationContext(), "Hasło musi mieć przynajmniej 8 znaków", Toast.LENGTH_SHORT).show();
                            return;
                        }
                    } else {
                        System.out.println("Niepoprawny email");
                        Toast.makeText(getApplicationContext(),"Niepoprawny adres email!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                }
            }
        });

        TextView loginScreen = (TextView) findViewById(R.id.link_to_login);
        loginScreen.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                // Switching to Register screen
                Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(i);
            }
        });


    }


    void register() {
        userName.setError(null);
        regPass.setError(null);
        regEmail.setError(null);
        regPassRepeated.setError(null);
        focusView = regEmail;
        /**
         * Sprawdzamy czy proces logowania przebiegł bez błędów.
         * Jeśli cancel = true, oznacza to, że wystąpiły jakieś błędy i proces logowania zostanie przerwany. Użytkownik zostanie poproszony o poprawienie danych.
         * Jesli cancel = false, oznacza to, że nie wystąpił żadne błędy w procesie logowania i zostanie otworzona kolejna karta - Overview.
         */
        if (cancel) {
            System.err.println("Rejestracja nieudana. Spróbuj ponownie.");
            Communicate.closeConnection();
            mRegTask = null;
            cancel = false;
            focusView.requestFocus();
            return;

        } else {
           if(registration_is_successful){
                Intent intentLogin = new Intent(RegisterActivity.this, LoginActivity.class);
                startActivity(intentLogin);
                Communicate.closeConnection();
                finish();
            }
            else{
                System.out.println("Rejestracja nieudana. Spróbuj jeszcze raz!");
                Communicate.closeConnection();
                mRegTask = null;
            }
        }
    }


public class UserRegisterTask extends AsyncTask<Void, Void, Boolean> {

    private final String mEmail;
    private final String mUsername;
    private final String mPassword;

    UserRegisterTask(String email, String nick, String password) {
        mEmail = email;
        mUsername = nick;
        mPassword = password;
    }

    Boolean s = false;
    Boolean l = false;
    Boolean c = false;
    Boolean p = false;
    @Override
    protected Boolean doInBackground(Void... params) {
        try {
            //Checking if server is running
            if (isServerRunning()) {
                try {
                    s = Communicate.createConnection();
                } catch (IOException e) {
                    Toast.makeText(getApplicationContext(), "Serwer nie odpowiada!", Toast.LENGTH_SHORT).show();
                }
            } else {
                System.err.println("Serwer uruchomiony. Połączenie udane.");
            }
        } catch (IOException e) {
            System.err.println("Blad polaczenia z serwerem!");
            Toast.makeText(getApplicationContext(), "Serwer nie odpowiada!", Toast.LENGTH_SHORT).show();
        }
        if (s) {
            l = DatabaseQueries.isEmailRegistered(mEmail);
            if (!l) {
                c = DatabaseQueries.isUsernameFree(mUsername);
                if(c)
                {
                    p = DatabaseQueries.sendPassword(mPassword);
                    if(p){
                        registration_is_successful = true;
                    }
                }
                else{
                    return c;
                }
            } else {
                return l; }
        } else {
            return s; }
        return p;
    }

    @Override
    protected void onPostExecute(final Boolean s) {
        if (l){
            Toast.makeText(getApplicationContext(), "Adres email jest już zarejestrowany!", Toast.LENGTH_SHORT).show();
            cancel = true;
            mRegTask=null;
            l=true;
        }
        else if(!c) {
            Toast.makeText(getApplicationContext(), "Wybrany nick jest już zajęty!!", Toast.LENGTH_SHORT).show();
            cancel = true;
            mRegTask=null;
            c=true;
        }
        else if(!s){
            Toast.makeText(getApplicationContext(), "Błąd połaczenia", Toast.LENGTH_SHORT).show();
            cancel = true;

        }
        register();
    }

    @Override
    protected void onCancelled() {
        mRegTask = null;
    }

}
}



package it.zabinska.studentcompanion;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;

public class SubjectActivity extends AppCompatActivity {
static String chosenSubject="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subject);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        ArrayAdapter<String> subjectsArray= new ArrayAdapter<String>(SubjectActivity.this,android.R.layout.simple_spinner_item, ProvisionalValidation.subjectsall);
        subjectsArray.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        final Spinner subjects = (Spinner) findViewById(R.id.subjectSpinner);
        subjects.setAdapter(subjectsArray);

        Button newGrade = (Button) findViewById(R.id.assign_subject);
        newGrade.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chosenSubject = subjects.getSelectedItem().toString();
                UserSubjectTask ust = new UserSubjectTask();
                ust.execute();
            }
        });
        updateUI();
    }
    void updateUI()
    {
        TableLayout table = (TableLayout) findViewById(R.id.subjectTable);
        for(int i=1; i<=ProvisionalValidation.sAmount; i++) {
            TableRow row = new TableRow(this);
            row.setId(666 + i);
            row.setLayoutParams(new TableRow.LayoutParams());
            TextView przedmiot = new TextView(this);
            przedmiot.setText(ProvisionalValidation.subjects[i-1]);
            row.addView(przedmiot);
            TextView średnia = new TextView(this);
            średnia.setText(ProvisionalValidation.grades[i][0]); //w docelowej wersji wyśeitlanie średniej
            row.addView(średnia);
            table.addView(row);
        }
    }
    public class UserSubjectTask extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                Boolean c = Communicate.createConnection();
                if (c) {
                    try {
                        DatabaseQueries.bindSubjectToUser(chosenSubject);
                        Communicate.closeConnection();
                        return true;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            return false;
        }
        protected void onPostExecute(Boolean s) {
            if (s) {
                updateUI();
            } else {
                Toast.makeText(getApplicationContext(), "Nie udalo sie dodac oceny!", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
